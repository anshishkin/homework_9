from posix import EX_PROTOCOL
from posixpath import sep
from flask import Flask,jsonify,request,abort,Response
import json,requests
from mlflow.tracking import client
import sys
import time
from requests.exceptions import HTTPError
import os
from flask_apscheduler import APScheduler
from os import environ as env
from dotenv import load_dotenv
load_dotenv()
from sklearn.datasets import load_iris
import mlflow
from mlflow.pyfunc import PythonModel
from sklearn.ensemble import RandomForestRegressor
from urllib.parse import urlparse
from sklearn.datasets import load_iris
import pickle
os.environ['AWS_ACCESS_KEY_ID'] = "minio"
os.environ['AWS_SECRET_ACCESS_KEY'] ="pass"
#os.environ['MLFLOW_TRACKING_URI']="http://95.217.188.148:5000"
#os.environ['MLFLOW_TRACKING_URI']="http://0.0.0.0:5000"
os.environ['MLFLOW_S3_ENDPOINT_URL']="http://s3:9000"
url=env['URL']
#client=mlflow.tracking.MlflowClient()
data = load_iris()


# class Model(PythonModel):

#     def __init__(self, n):
#         self.n = n
#         self.model=None

#     def fit(self):
#         iris = load_iris()
#         self.model = RandomForestRegressor().fit(iris.data, iris.target)
#         return self.model
    
#     def predict(self, data):
#         return self.model.predict(data)
         


def mlflow():
    iris = load_iris()
    mlflow.set_tracking_uri(env['MLFLOW_TRACKING_URI'])
    try:
        experiment_id = mlflow.create_experiment(name="hardML")
    except:
        experiment_id = mlflow.get_experiment_by_name(name="hardML").experiment_id

    mlflow_run = mlflow.start_run(experiment_id=experiment_id,run_name=f"run_rf")
    #model=Model(n=100)
    run_id = mlflow.active_run().info.run_id
    run = client.get_run(run_id)
    with mlflow_run:
        rf=RandomForestRegressor(n_estimators=100)
        rf.fit(iris.data[:,:2],iris.target)
        mlflow.sklearn.log_model(rf,'random_f')
        mlflow.sklearn.save_model(path="model_rf", 
                               python_model=rf,
                               #artifacts=artifacts
                              )
    return run_id,run
def save_model(model):
        with open('model.pkl', 'wb') as f:
            pickle.dump(model, f)
        print("Model saved .... Done! \n")
def create_app():
    #run_id,run=mlflow()
    iris = load_iris()

    rf=RandomForestRegressor(n_estimators=100)
    rf.fit(iris.data[:,:2],iris.target)
    save_model(rf)
    app= Flask(__name__)
    time.sleep(1)
    
    @app.route("/get_source_iris_pred")
    
    def exec_source():   
        
        #response=requests.get(url,verify=False)
        #jsonResponse = response.json()
        #model=mlflow.sklearn.load_model(f'http://s3:9000/{run_id}/{run}/artifacts/model')        
        iris = load_iris()
        sepal_length = request.args.get('sepal_length', default = 1, type = float)
        sepal_width=request.args.get('sepal_width', default = 1, type = float)
        prediction=RandomForestRegressor(n_estimators=100).fit(iris.data[:,:2],iris.target).predict([[sepal_length,sepal_width]])
        #print(prediction)
        output=jsonify({"prediction":float(prediction)})
        return output
    
    @app.route("/get_string_iris_pred")
    
    # def exec_string():   
    #     abort(501)
    def exec_string():   
        
        response=requests.get(url,verify=False)
        jsonResponse = response.json()
        #model=mlflow.sklearn.load_model(f'http://s3:9000/{run_id}/{run}/artifacts/model')        
        iris = load_iris()
        sepal_length = request.args.get('sepal_length', default = 1, type = float)
        sepal_width=request.args.get('sepal_width', default = 1, type = float)
        prediction=RandomForestRegressor(n_estimators=100).fit(iris.data[:,:2],iris.target).predict([[sepal_length,sepal_width]])
        trh_low=jsonResponse['threshold_0_1']
        trh_up=jsonResponse['threshold_1_2']
        if prediction<trh_low:
            kind_iris='setosa'
            class_iris=1
        elif prediction>trh_up:
            kind_iris='virginica'
            class_iris=3
        else: 
            kind_iris='versicolor'
            class_iris=2
        
        output=jsonify({"class": class_iris, 
                        "class_str": kind_iris, #<setosa|versicolor|virginica>,
                        "threshold_0_1": trh_low, 
                        "threshold_1_2": trh_up})
        return output
    
    return app
    

app=create_app()

if __name__=='__main__':

    app.run(host='0.0.0.0',port='8002',debug=True)
    
